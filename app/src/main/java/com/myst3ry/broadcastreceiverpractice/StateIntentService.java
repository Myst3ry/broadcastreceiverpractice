package com.myst3ry.broadcastreceiverpractice;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

public final class StateIntentService extends IntentService {

    private static final int START_POSITION = 0;

    public StateIntentService() {
        super(StateIntentService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            final String stateName = intent.getStringExtra(IntentConstants.EXTRA_STATE);
            if (IntentConstants.ACTION_CHANGE_STATE.equalsIgnoreCase(action)) {
                handleChangeState(stateName);
            }
        }
    }

    private void handleChangeState(final String stateName) {
        int pos = getCurrentState(stateName).ordinal();
        if (++pos >= State.values.length) {
            pos = START_POSITION;
        }

        final State nextState = State.values[pos];
        changeCurrentState(nextState);
        sendStateChangedInfo(nextState.name());
    }

    private State getCurrentState(final String stateName) {
        return State.valueOf(stateName);
    }

    private void changeCurrentState(final State state) {
        StateManager.getInstance().changeState(state);
    }

    private void sendStateChangedInfo(final String stateName) {
        final Intent intent = new Intent();
        intent.setAction(IntentConstants.ACTION_CHANGE_STATE);
        intent.putExtra(IntentConstants.EXTRA_STATE, stateName);
        intent.setFlags(Intent.FLAG_INCLUDE_STOPPED_PACKAGES);
        sendBroadcast(intent);
    }

    public static Intent getIntentForSend(final Context context, final String state) {
        final Intent intentForSend = newIntent(context);
        intentForSend.setAction(IntentConstants.ACTION_CHANGE_STATE);
        intentForSend.putExtra(IntentConstants.EXTRA_STATE, state);
        return intentForSend;
    }

    public static Intent newIntent(final Context context) {
        return new Intent(context, StateIntentService.class);
    }
}
