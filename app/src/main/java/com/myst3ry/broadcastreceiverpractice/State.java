package com.myst3ry.broadcastreceiverpractice;

public enum State {
    INITIALIZE, CREATE, START, UPDATE, STOP, DESTROY, DELETE;
    public static final State values[] = values();
}
