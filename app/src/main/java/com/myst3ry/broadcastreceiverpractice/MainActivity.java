package com.myst3ry.broadcastreceiverpractice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends AppCompatActivity {

    @BindView(R.id.text)
    TextView textView;
    @BindView(R.id.btn)
    Button button;

    private BroadcastReceiver receiver;
    private IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initListeners();
        init();
    }

    private void initListeners() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIntentService(textView.getText().toString());
            }
        });
    }

    private void init() {
        receiver = new StateBroadcastReceiver();
        filter = new IntentFilter(IntentConstants.ACTION_CHANGE_STATE);
        textView.setText(State.values[0].name());
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    private void startIntentService(final String stateName) {
        startService(StateIntentService.getIntentForSend(this, stateName));
    }

    private final class StateBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String newState = intent.getStringExtra(IntentConstants.EXTRA_STATE);
            textView.setText(newState);
        }
    }
}
