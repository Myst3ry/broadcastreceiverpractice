package com.myst3ry.broadcastreceiverpractice;

public final class IntentConstants {

    public static final String ACTION_CHANGE_STATE = BuildConfig.APPLICATION_ID + ".action.CHANGE_STATE";
    public static final String EXTRA_STATE = BuildConfig.APPLICATION_ID + ".extra.EXTRA_STATE";

    private IntentConstants() {}
}
