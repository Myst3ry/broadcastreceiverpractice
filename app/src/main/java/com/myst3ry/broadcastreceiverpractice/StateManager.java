package com.myst3ry.broadcastreceiverpractice;

public final class StateManager {

    private static final StateManager OUT_INSTANCE = new StateManager();
    private State state;

    public static StateManager getInstance() {
        return OUT_INSTANCE;
    }

    private StateManager() {
        state = State.INITIALIZE;
    }

    public State getState() {
        return state;
    }

    public void changeState (State state) {
        this.state = state;
    }
}

